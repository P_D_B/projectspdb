package tel;

public class DeLinea extends Telefono {
	
	private String due�o;
	private String numeroserie;
	private String timbre="TUUU-TUUU";

	public DeLinea(String marca, String color,String due�o,String numeroserie) {
		super(marca, color);
		this.numeroserie = numeroserie;
		this.due�o = due�o; 
		
		// TODO Auto-generated constructor stub
	}

	public String getUbicacion() {
		return due�o;
	}

	public void setUbicacion(String ubicacion) {
		this.due�o = ubicacion;
	}

	public String getNumeroserie() {
		return numeroserie;
	}

	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}
	
	public void Sonar() {
		System.out.println(this.timbre);
	}

	
	public String toString() {
		return "DeLinea [due�o=" + due�o + ", numeroserie=" + numeroserie + ", timbre=" + timbre + "]";
	}
}
