package tel;

public class Celular extends Telefono {
       private String modelo;
       private int numero;
       private String timbre="Sonido-Polifonico";
       
	public Celular(String marca, String color, String modelo,int numero) {
		super(marca, color);
		this.modelo = modelo;
		this.numero = numero;		
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTimbre() {
		return timbre;
	}

	public void setTimbre(String timbre) {
		this.timbre = timbre;
	}
	
	public void Sonar() {
		System.out.println(this.timbre);
	}

	public String toString() {
		return "Celular \n [modelo=" + modelo +  "\n numero=" + numero + "\n timbre=" + timbre + "]";
	}

}
