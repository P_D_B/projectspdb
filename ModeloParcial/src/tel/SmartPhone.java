package tel;

public class SmartPhone extends Celular {
	   private String SO;
	   private String timbre="Ring-tone";

	public SmartPhone(String marca, String color, String modelo, int numero,String SO) {
		super(marca, color, modelo, numero);
		this.SO = SO;
		
	}

	public String getSO() {
		return SO;
	}

	public void setSO(String sO) {
		SO = sO;
	}

	public void Sonar() {
		System.out.println(this.timbre);
	}

	
	public String toString() {
		return "SmartPhone [SO=" + SO + ", timbre=" + timbre + "]";
	}
    
   public boolean equals(Celular c) {
	      boolean result = false ;
	      
             if ((c != null) && (c instanceof SmartPhone)) {
            	 SmartPhone s = (SmartPhone) c ; 
                if ((s.SO == this.SO) && (s.timbre == this.timbre) && (c.getColor()== this.getColor())) {
                	result = true;
                 }
            
              }
			return result;
          }
   
}
   
	

