package tel;

public class Telefono {

	private String marca;
	private String color;
	private String timbre="RING!";

	
	public Telefono (String marca,String color) {
	super();	
    this.marca = marca;
    this.color = color;
    	
	}

	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}
	
	public void Sonar() {
	System.out.println(this.timbre);
	}

	
	public String toString() {
		return "Telefono [marca=" + marca + ", color=" + color + ", timbre=" + timbre + "]";
	}
	
}
