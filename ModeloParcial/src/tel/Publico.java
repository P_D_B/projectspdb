package tel;

public class Publico extends Telefono {
	
	private String ubicacion;
	private String timbre="RING---RING----RING"; 
	
	public Publico(String marca, String color, String ubicacion ) {
		super(marca, color);
		this.ubicacion = ubicacion;
		
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public void Sonar() {
		System.out.println(this.timbre);
	}

	
	public String toString() {
		return "Publico [ubicacion=" + ubicacion + ", timbre=" + timbre + "]";
	}
}

