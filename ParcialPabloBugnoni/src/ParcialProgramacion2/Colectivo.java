package ParcialProgramacion2;

public class Colectivo extends Pasajeros {
	   private String linea;
       private String chofer;
         
		public Colectivo(String marca, String modelo, String capacidad, int puertas,String linea,String chofer) {
		super(marca, modelo, capacidad, puertas);
		this.chofer = chofer;
		this.linea = linea;
	}




	public String getLinea() {
		return linea;
	}




	public void setLinea(String linea) {
		this.linea = linea;
	}




	public String getChofer() {
		return chofer;
	}




	public void setChofer(String chofer) {
		this.chofer = chofer;
	}




	
	public String toString() {
		return "Colectivo \n [linea=" + linea + ", chofer=" + chofer + ", getCapacidad()=" + getCapacidad()
				+ ", getPuertas()=" + getPuertas() + ", getMarca()=" + getMarca() + ", getModelo()=" + getModelo()
				+"]";
	}




	
}
