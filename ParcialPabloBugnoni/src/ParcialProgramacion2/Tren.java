package ParcialProgramacion2;

public class Tren extends Pasajeros{
	 private int vagones;
     private String linea;
     
	 public Tren(String marca, String modelo, String capacidad, int puertas,int vagones,String linea ) {
		super(marca, modelo, capacidad, puertas);
        this.linea = linea;
        this.vagones = vagones;
	}

	public int getVagones() {
		return vagones;
	}

	public void setVagones(int vagones) {
		this.vagones = vagones;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	@Override
	public String toString() {
		return "Tren \n [vagones=" + vagones + ", linea=" + linea + ", getCapacidad()=" + getCapacidad()
				+ ", getPuertas()=" + getPuertas() + ", getMarca()=" + getMarca()
				+ ", getModelo()=" + getModelo() + "]";
	}
       
	

}