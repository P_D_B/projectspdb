package ParcialProgramacion2;

public class Automovil extends Personal {
	private String due�o;
	
	public Automovil(String marca, String modelo, String dominio,String due�o) {
		super(marca, modelo, dominio);
		this.due�o = due�o;
	}


	
	public String getDue�o() {
		return due�o;
	}
	public void setDue�o(String due�o) {
		this.due�o = due�o;
	}
	
	public String toString() {
		return "Automovil \n [due�o=" + due�o + ", getDominio()=" + getDominio() + ", getMarca()=" + getMarca()
				+ ", getModelo()=" + getModelo() +"]";
	}
	
	
	   public boolean equals(Personal p) {
		      boolean result = false ;
		      
	             if ((p != null) && (p instanceof Automovil)) {
	            	 Automovil A = (Automovil) p ; 
	                if ((A.due�o == this.due�o)&&(p.getDominio()==this.getDominio()&&(p.getMarca()==this.getMarca())) )  {
	                	result = true;
	                 }
	            
	              }
				return result;
	          }
	   
	
	
}
