package ParcialProgramacion2;

public class Personal extends Vehiculo{
        private String dominio;
        
	    public Personal(String marca, String modelo,String dominio) {
		super(marca, modelo);
        this.dominio = dominio;
	}

		public String getDominio() {
			return dominio;
		}

		public void setDominio(String dominio) {
			this.dominio = dominio;
		}

		public String toString() {
			return "Personal [dominio=" + dominio + ", getMarca()=" + getMarca() + ", getModelo()=" + getModelo()
					 + "]";
		}

}
