package ParcialProgramacion2;


public class Test {

	public static void main(String[] args) {
		Automovil p = new Automovil("fiat", "mobi", "XXXXXX", "perez");
		Automovil p1 = new Automovil("fiat", "mobi", "XXXXXX", "perez");
		System.out.println(p.equals(p1)); 			  
		  TablaV T = new TablaV();
	      T.recorrer();
		  

	}

}


//La herencia en java es cuando a travez de la relacion entre super clase y sub clase (es un)
//una clase padre le hereda a su clase hija estados y comportamientos.
//El polimorfismo en java es una estrategia mediante la cual se le pasa un mensaje a una clase y esta 
//se comporta de manera diferente, va de mano con la sobre escritura, la herencia y la sobrecarga. un ejemplo
//claro en este programa es el metodo toString q en cada clase se comporta de manera diferente al ser invocado desde un for each dentro de un array.