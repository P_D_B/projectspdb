package ParcialProgramacion2;

public class Motocicleta extends Personal {
	
	private String due�o; 
	
	public Motocicleta(String marca, String modelo, String dominio, String due�o) {
		super(marca, modelo, dominio);
		this.due�o = due�o; }
		
	public String getDue�o() {
		return due�o;
	}
	public void setDue�o(String due�o) {
		this.due�o = due�o;
	}

	public String toString() {
		return "Motocicleta \n [due�o=" + due�o + ", getDominio()=" + getDominio() + ", getMarca()=" + getMarca()
				+ ", getModelo()=" + getModelo() + "]";
	}

}
