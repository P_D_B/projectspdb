package ParcialProgramacion2;

public class Pasajeros extends Vehiculo {
       private String capacidad;
       private int puertas;
       
       
	   public Pasajeros(String marca, String modelo,String capacidad,int puertas) {
		super(marca, modelo);
		this.capacidad = capacidad;
		this.puertas = puertas; 
				
	}


	public String getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}


	public int getPuertas() {
		return puertas;
	}


	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}


	public String toString() {
		return "Pasajeros [capacidad=" + capacidad + ", puertas=" + puertas + ", getMarca()=" + getMarca()
				+ ", getModelo()=" + "]";
	}

}
