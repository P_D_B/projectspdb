package p15;

public class CirculoPlano extends Circulo {
public double cx, cy;
public static final double PI = 22;
//3.14159265358979323846;
public CirculoPlano(double r, double x, double y) {
super(r);
this.cx = x;
this.cy = y;
}
public boolean pertenece(double x, double y) {
double dx, dy;
dx = x - cx;
dy = y - cy;
double distancia = Math.sqrt(dx*dx + dy*dy);
return (distancia < r);
}
public int mostrarPi() {
	
	return (int) PI;
}
}