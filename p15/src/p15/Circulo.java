package p15;

public class Circulo {
;
		public static final double PI=3.14159;
		public double r;
		public Circulo(double r) {
		this.r = r;
		}
		public static double radianesAgrados(double rads) {
		return (rads * 180 / PI);
		}
		public double area() {
		return (PI * r * r);
		}
		public double circunferencia() {
		return (2 * PI * r);
		}
}
