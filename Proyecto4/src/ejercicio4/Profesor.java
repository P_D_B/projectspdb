package ejercicio4;

public class Profesor {

	private static final Integer BASE = 10;
	private String apellido;
	private String nombre;
	private String asignatura;

	public void ponerNotas(Alumno alumno) {
		
		ponerNota(alumno.getM1()) ;
		ponerNota(alumno.getM2()) ;
		ponerNota(alumno.getM3()) ;
		
		//alumno.getM1().setCalificacion(Math.random() * BASE); // alias objeto + traer atributo + metodo setter de asignatura + dentro de asignatura (metodo consigna)
		//alumno.getM2().setCalificacion(Math.random() * BASE);
		//alumno.getM3().setCalificacion(Math.random() * BASE);
	}
	
	private void ponerNota (Asignatura asignatura) {
	
		asignatura.setCalificacion(Math.random() * BASE);
		
	}
	public double calcularPromedio (Alumno alumno) {

		return (alumno.getM1().getCalificacion() + alumno.getM2().getCalificacion() + alumno.getM3().getCalificacion())/3;
		
		
	}

}
